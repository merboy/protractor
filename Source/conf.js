var jasmineReporters = require('jasmine-reporters');
var env = process.env.NODE_ENV;

exports.config = {
  directConnect: true,
  capabilities: {
    'browserName': 'chrome',
  },
  framework: 'jasmine',
  suites: {
    interact: ['login.js','interact.js','feedback.js'],
    skips: ['login.js','interact.js','skips.js'],
    idle: ['login.js','idle.js'],
    emoticons: ['login.js','emoticons.js'],
  },

  jasmineNodeOpts: {
    defaultTimeoutInterval: 900000//60000
  },
  onPrepare: function() {
      jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
          consolidateAll: true,
          savePath: './Reports/',
          filePrefix: 'xmlresults'
      }));
   },
  onComplete: function() {
     var browserName, browserVersion;
     var capsPromise = browser.getCapabilities();
 
     capsPromise.then(function (caps) {
        browserName = caps.get('browserName');
        browserVersion = caps.get('version');
        var HTMLReport = require('protractor-html-reporter');
 
        testConfig = {
            reportTitle: 'Total Rewards Bot - Test Automation',
            outputPath: './Reports/',
            testBrowser: browserName+' - '+env,
            browserVersion: browserVersion,
            modifiedSuiteName: false,
        };
        new HTMLReport().from('./Reports/xmlresults.xml', testConfig);
    });
  }
};

