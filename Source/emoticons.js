var userCredentials = require('../Config/UserLogin.json')
var objects = require('../Config/pageObjects.js')
var func = require('../Config/functions.js')
 var testdata = require('../Config/testdata.json')
var originalTimeout;
var count = 0;

describe('Use Emoticons to interact with bot', function() {
	
	beforeEach(function() {
		browser.waitForAngularEnabled(false);
		browser.sleep(16000);
    });

    it('should display greetings', async function(){
    	func.validateTexts(objects.getBotMessage(), testdata.greetings, count, testdata.wb);
    	count = await func.confirmResponse(objects.getBotMessage(), count);
	});

	it('should respond :D', function(){
		objects.setUserInput(testdata.emoticon[0]);
		objects.clickSendInputbtn();
	});

	it('should validate response on :D', async function(){
		func.validateTexts(objects.getBotMessage(), testdata.emoticonResponse1, count, testdata.wb);
    	count = await func.confirmResponse(objects.getBotMessage(), count);
	});
	it('should respond <3', function(){
		objects.setUserInput(testdata.emoticon[1]);
		objects.clickSendInputbtn();
	});

	it('should validate response on <3', async function(){
		func.validateTexts(objects.getBotMessage(), testdata.emoticonResponse2, count, testdata.wb);
    	count = await func.confirmResponse(objects.getBotMessage(), count);
	});
	it('should respond :(', function(){
		objects.setUserInput(testdata.emoticon[2]);
		objects.clickSendInputbtn();
	});

	it('should validate response on :(', async function(){
		func.validateTexts(objects.getBotMessage(), testdata.emoticonResponse3, count, testdata.wb);
    	count = await func.confirmResponse(objects.getBotMessage(), count);
	});
	it('should respond :)', function(){
		objects.setUserInput(testdata.emoticon[3]);
		objects.clickSendInputbtn();
	});

	it('should validate response on :(', async function(){
		func.validateTexts(objects.getBotMessage(), testdata.emoticonResponse4, count, testdata.wb);
    	count = await func.confirmResponse(objects.getBotMessage(), count);
	});

	it('should respond :/', function(){
		objects.setUserInput(testdata.emoticon[4]);
		objects.clickSendInputbtn();
	});

	it('should validate response on :/', async function(){
		func.validateTexts(objects.getBotMessage(), testdata.emoticonResponse5, count, testdata.wb);
    	count = await func.confirmResponse(objects.getBotMessage(), count);
	});


});
		
		
		
		
		

