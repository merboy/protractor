var userCredentials = require('../Config/UserLogin.json')
var objects = require('../Config/pageObjects.js')
var func = require('../Config/functions.js')
var testdata = require('../Config/testdata.json')
var count = 0;

describe('Interact with Bot - validate responses, emoticons, buttons', function() {
	
	beforeEach(function() {
		browser.waitForAngularEnabled(false);
		browser.sleep(15000);	
    });
	
	it('should validate greeting message', async function(){
		func.validateTexts(objects.getBotMessage(), testdata.greetings,count, testdata.wb);
		count = await func.confirmResponse(objects.getBotMessage(), count);		
	});

	it('should ask Question 1', function(){
		objects.setUserInput(testdata.userInput[0]);
		objects.clickSendInputbtn();
	});

	it('should validate and expand response, and click sad emoticon', async function(){
		count = await func.confirmResponse(objects.getBotMessage(), count);
		func.expand(objects.getExpandAnswerBtn());
		objects.clickEmoji('sad');
	});

	it('should confirm response on sad emoticon', async function(){
		count = await func.confirmResponse(objects.getBotMessage(), count);
	});

	it('should ask Question 2', function(){
		objects.setUserInput(testdata.userInput[1]);
		objects.clickSendInputbtn();
	});

	it('should confirm response, and click smiley', async function(){
		count = await func.confirmResponse(objects.getBotMessage(), count);
		objects.clickEmoji('happy');
	});

	it('should select YES for additional inquiry', function(){	
		objects.clickButton('yes');
	});

	it('should confirm response, and ask Question 3', async function(){
		count = await func.confirmResponse(objects.getBotMessage(), count);
		objects.setUserInput(testdata.userInput[2]);
		objects.clickSendInputbtn();
	})

	it('should confirm response,and click smiley', async function(){
		count = await func.confirmResponse(objects.getBotMessage(), count);
		objects.clickEmoji('happy');
	});

	it('should select NO additional inquiry', function(){
		objects.clickButton('no');
	});
	
	it('should rate bot', function(){
		objects.rateBot();
	});

});
		
		
		
		
		

