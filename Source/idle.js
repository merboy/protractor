var userCredentials = require('../Config/UserLogin.json')
var objects = require('../Config/pageObjects.js')
var func = require('../Config/functions.js')
 var testdata = require('../Config/testdata.json')
var originalTimeout;
var count = 0;

describe('Idle for 10 minutes', function() {
	
	beforeEach(function() {
		browser.waitForAngularEnabled(false);
		browser.sleep(20000);
    });

    it('should display greetings', async function(){
    	func.validateTexts(objects.getBotMessage(), testdata.greetings, count,testdata.wb);
    	count = await func.confirmResponse(objects.getBotMessage(), count);
	});

	it('should timeout', function(){
		browser.sleep(800000);
	});

	it('should validate timeout message', function(){
		func.validateTexts(objects.getBotMessage(), testdata.iddleMessage, count-1, testdata.wb);
	});
});
		
		
		
		
		

