var userCredentials = require('../Config/UserLogin.json')
var objects = require('../Config/pageObjects.js')
var func = require('../Config/functions.js')
var testdata = require('../Config/testdata.json')
var count = 0;

describe('Skip buttons, emoticons, ratings, and feedback', function() {
	
	beforeEach(function() {
		browser.waitForAngularEnabled(false);
		browser.sleep(18000);	
    });


	// DISABLED - FEEDBACK
	it('should skip and ask', function(){
		objects.setUserInput('ask');
		objects.clickSendInputbtn();
	});

	it('should confirm feedback is DISABLED', function(){
		func.confirmDisabled(objects.getFeedback(), 'one');
	});

	it('should ask Question 4', function(){
		objects.setUserInput(testdata.userInput[3]);
		objects.clickSendInputbtn();
	});

	it('should respond YES instead of smiley', function(){
		objects.setUserInput('YES');
		objects.clickSendInputbtn();
	});
	// DISABLED - EMOJI
	it('should confirm emojis are DISABLED', function(){
		func.confirmDisabled(objects.getEmojis(), 'all');
	});

	it('should respond NO instead of clicking the button', function(){
		objects.setUserInput('NO');
		objects.clickSendInputbtn();
	});

	// DISABLED - BUTTONS
	it('should confirm yes/no buttons are DISABLED', function(){
		func.confirmDisabled(objects.getButtons(), 'all');
	});

	it('should skip and ask', function(){
		objects.setUserInput('ask');
		objects.clickSendInputbtn();
	});
	// DISABLED - RATING
	it('should confirm RATE STARS are DISABLED', function(){
		func.confirmDisabled(objects.getRating(), 'all');
	});

});
		
		
		
		
		

