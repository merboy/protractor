var pageObjects = function() {
	//login 
	var username = element(by.xpath('//*[@id="userNameInput"]'));
	var password = element(by.xpath('//*[@id="passwordInput"]'));
	var loginBtn = element(by.xpath('//*[@id="submitButton"]'));
	
	/* var usernmae = expect(browser.driver.wait(function () {
            return browser.driver.findElement(by.xpath('//*[@id="userNameInput"]')).isDisplayed();
        })).toBe(true); */
	
	//bot's messages
	var userInput = element(by.tagName('input'));
	var sendInputbtn = element(by.className('send-button'));
	var botMessage = element.all(by.tagName('li[_ngcontent-c1]'));
	var msgs = [];
	var expandAnswerBtn = element.all(by.className('message-buttons expand-toggle-button arrow-down-icon'));

	//user satisfaction buttons
	var smileyEmoji = element(by.className('thumbsButtons thumbsUp'));
	var sadEmoji = element(by.className('thumbsButtons thumbsDown'));
	var emojiButtons = element.all(by.className('message-buttons quickSuggestButtons tr-emojis-feedback'));
	
	//answers when bot asks if user has additional inquiry
	var yesNoBtn = element.all(by.className('message-buttons quickSuggestButtons tr-acceptance-feedback'));
	//var noBtn = element(by.className('message-buttons quickSuggestButtons tr-acceptance-feedback'));
	
	//feedback and evaluation
	var starRatings = element.all(by.className('tr-clickable-rating message-buttons quickSuggestButtons rating1Back tr-inactive-rating'));
	var inputFeedback = element(by.name('feedback'));
	var sendFeedbackBtn = element(by.className('tr-feedback-submit ng-untouched ng-pristine ng-valid'));
    

    this.getBrowser = function() {
		browser.get('https://...../');
	};
	
	this.login = function(userName, ps){
		username.sendKeys(userName);
		password.sendKeys(ps);
		return loginBtn.click();
	};
	
	this.getBotMessage = function() {
		return botMessage;
	}; 

	this.getSadEmoji = function() {
		return sadEmoji;
	}; 

	this.getHappyEmoji = function() {
		return smileyEmoji;
	}; 

	this.getEmojis = function(){
		return emojiButtons;
	}

	this.getButtons = function(){
		return yesNoBtn;
	}

	this.getFeedback = function(){
		return inputFeedback;
	}

	this.getRating = function(){
		return starRatings;
	}




	this.setUserInput = function(input) {
		userInput.sendKeys(input);
	};

	this.clickSendInputbtn = function() {
		return sendInputbtn.click();
	};
	
	this.getExpandAnswerBtn = function() {
		return expandAnswerBtn;
	};
	this.clickEmoji = function(emoji) {
		if(emoji == 'sad')
			return sadEmoji.click();
		else
			return smileyEmoji.click();
	};
	this.clickButton = function(btn) {
		if(btn == 'yes')
			yesNoBtn.first().click();
		else
			yesNoBtn.last().click();
	};
	this.rateBot = function(){
		return starRatings.last().click();
	}
	this.SetInputFeedback = function(feedback) {
		inputFeedback.sendKeys(feedback);
	};
	this.clickSendFeedback = function() {
		return sendFeedbackBtn.click();
	};
};

module.exports = new pageObjects();