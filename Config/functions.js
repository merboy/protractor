var func = function() {

	this.validateTexts = function(elements, testdata, count, wb){
		var innerCount = 0;
		var dataCount = 0;
		elements.each(function(message){
			message.getText().then(function(value){
				if(value == ""||value == "_" || innerCount < count){
					console.log("Skipping >> ", value);
				}else if(value.includes("Welcome back")){
					expect(wb[0] == value ).toBe(true);
				}
				else{
					expect(testdata[dataCount].option1 == value || testdata[dataCount].option2 == value || testdata[dataCount].option3 == value).toBe(true, "ACTUAL > "+value+"\nEXPECTED Option 1 >"+testdata[dataCount].option1+"\nEXPECTED Option 2>"+testdata[dataCount].option2+"\nEXPECTED Option 3>"+testdata[dataCount].option3);
					dataCount++;
					count++;
				}
				innerCount++;
			});
		});
	}
	this.confirmResponse = function(elements, count){
		return elements.count().then(function(elemCount){
			expect(elemCount).toBeGreaterThan(count);
			return elemCount;
		});


	}
	this.displayTexts = function(elements, count){
		return elements.count().then(function(elemCount){
			for(;count < elemCount ; count++){
				elements.get(count).getText().then(function(value){
					console.log("displayTexts >>", value);
				});
			}
			return elemCount
		});
	}
	this.expand = function(elements){
		elements.each(function(btn){
			btn.click();
		});
	}

	this.confirmDisabled = function(elem, num){
		if(num === 'one'){
			expect(elem.isEnabled()).toBe(false);
		}
		else if (num === 'all'){
			elem.each(function(elem){
				expect(elem.isEnabled()).toBe(false);
			})
		}
	}
};

module.exports = new func();